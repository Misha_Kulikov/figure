public class Square extends Shape {
    private Point corner;
    private double side;

    public Square(Color color, Point corner, double side) {
        super(color);
        this.corner = corner;
        this.side = side;
    }

    @Override
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Квадрат{" +
                "цвет = " + getColor() +
                "угол = " + corner +
                ", сторона = " + side +
                '}';
    }
}
