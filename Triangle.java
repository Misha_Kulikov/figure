public class Triangle extends Shape {
    private Point b;
    private Point a;
    private Point c;

    public Triangle(Color color, Point b, Point a, Point c) {
        super(color);
        this.b = b;
        this.a = a;
        this.c = c;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "цвет" + getColor() +
                "b=" + b +
                ", a=" + a +
                ", c=" + c +
                '}';
    }

    @Override
    public double area() {
        return Math.abs((a.getX() - c.getX()) * (b.getY() - c.getY()) * (b.getX() - c.getX()) * (a.getY() - c.getY())) / 2;
    }
}
